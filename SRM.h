#pragma once
/**
 * Scoped resource management library.
 * 
 * Liscense: MIT
 * Copyright: Ivane Gegia 2017
 */

#include <stdlib.h>
#include <iso646.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif    
    typedef int SRM_ScopeID;

    typedef void(*SRM_CleanupFunction)(void *resource);


    typedef enum
    {
        SRM_ERROR_NONE = 0,
        SRM_ERROR_COULDNT_ALLOCATE_MEMORY = 1,
        SRM_ERROR_NOT_ENOUGH_SPACE_ON_STACK = 2
    }SRM_ErrorCode;

    typedef struct
    {
        SRM_CleanupFunction _cleanupCallback;
        void* _resource;
        SRM_ScopeID _scopeId;
    } SRM_CleanupObject;

    typedef struct
    {
        SRM_CleanupObject* _cleanUpCallbackObjects;
        int _stackSize;
        int _lastItemIndex;
        SRM_ScopeID _currentScopeId;
    } SRM_Context;

    typedef struct
    {
        SRM_ErrorCode errorCode;
    } SRM_Error;
    
    SRM_Context* SRM_Context_Create(int stackSize, SRM_Error* error)
    {
        SRM_Context *ctx = (SRM_Context*)malloc(sizeof(SRM_Context));

        if (ctx == NULL)
        {
            if(NULL != error) error->errorCode = SRM_ERROR_COULDNT_ALLOCATE_MEMORY;

            return NULL;
        }

        ctx->_stackSize = stackSize;
        ctx->_lastItemIndex = -1;
        ctx->_currentScopeId = 0;
        ctx->_cleanUpCallbackObjects = (SRM_CleanupObject*)malloc(sizeof(SRM_CleanupObject)*ctx->_stackSize);

        if (NULL == ctx->_cleanUpCallbackObjects)
        {
            free(ctx);
            if (NULL != error) error->errorCode = SRM_ERROR_COULDNT_ALLOCATE_MEMORY;

            return NULL;
        }

        if (NULL != error) error->errorCode = SRM_ERROR_NONE;

        return ctx;
    }

    void SRM_Context_StartScope(SRM_Context *_this)
    {
        _this->_currentScopeId++;
    }

    void SRM_Context_RegisterCleanup(SRM_Context *_this, SRM_CleanupFunction callback, void* resource, SRM_Error *error)
    {
        if (_this->_stackSize == _this->_lastItemIndex)
        {
            if (NULL != error) error->errorCode = SRM_ERROR_NOT_ENOUGH_SPACE_ON_STACK;
            return;
        }

        _this->_lastItemIndex++;

        _this->_cleanUpCallbackObjects[_this->_lastItemIndex]._cleanupCallback = callback;
        _this->_cleanUpCallbackObjects[_this->_lastItemIndex]._resource = resource;
        _this->_cleanUpCallbackObjects[_this->_lastItemIndex]._scopeId = _this->_currentScopeId;

        
    }

    void SRM_Context_CloseScope(SRM_Context *_this)
    {
        while (true)
        {
            SRM_CleanupObject *cleanupObject = &_this->_cleanUpCallbackObjects[(_this->_lastItemIndex)];
                         
            if (_this->_lastItemIndex >= 0
                and _this->_currentScopeId == cleanupObject->_scopeId)
            {
                cleanupObject->_cleanupCallback(cleanupObject->_resource);

                _this->_lastItemIndex--;
            }
            else
            {
                _this->_currentScopeId--;
                break;
            }
        }
    }

    void SRM_Context_Free(SRM_Context *_this)
    {
        while (true)
        {
            if (_this->_lastItemIndex > 0)
            {
                SRM_Context_CloseScope(_this);
            }
            else
            {
                break;
            }
        }

        free(_this->_cleanUpCallbackObjects);
        free(_this);
    }

#ifdef __cplusplus
}
#endif
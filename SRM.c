﻿#include "SRM.h"
#include <stdio.h>

SRM_Context *srm_ctx1;

void SomeFunctionWithScopedResources();

int main()
{
    // Creating SRM context, this needs to be created 1 per thread.
    //                    This number indicates number of 
    //                            ↓ maximum resources to be managed.
    srm_ctx1 = SRM_Context_Create(10,NULL);
    
    // Some function which allocates or locks resources
    SomeFunctionWithScopedResources();

    // Deleting SRM system, it will aslow free any onclosed scopes.
    SRM_Context_Free(srm_ctx1);
    getchar();
    return 0;
}

// Resource handler function for scope exit
static void SomeFunctionWithScopedResources_freeInt(void *res){free(res);}
// Resource handler function for scope exit.
//           In this example it not only unlocks resource, ------------------------------------------------------\
//                                          it also does some shutdown work                                       |
//                                                                   ↓                                            ↓
static void SomeFunctionWithScopedResources_closeFile(void *res) {fprintf((FILE*)res, "%s", "closing file\n");fclose((FILE*)res);}
void SomeFunctionWithScopedResources()
{
    // Starting resource management scope
    // for specific tontext
    SRM_Context_StartScope(srm_ctx1);
    
    // Allocating integer on heap and registering function to be called on scope exit, to free up memory
    int *a = (int*)malloc(sizeof(int));//              |  and passing pointer to resource, to be managed.
                                       //              ↓                           ↓
    SRM_Context_RegisterCleanup(srm_ctx1, SomeFunctionWithScopedResources_freeInt, a, NULL);

    //     Allocating memory and registering function to free up the momory on scope exit later.
    //                                            |  resource pointer.
    char *blob1 = (char*)malloc(1024*1024*100);// ↓    ↓
    SRM_Context_RegisterCleanup(srm_ctx1,      free, blob1, NULL);

    // This is more complex example, where on scope exit, function not only closes file but
    // aslow write to the file before exit. See: SomeFunctionWithScopedResources_closeFile
    FILE *f1 = fopen(".\\somefile", "w+");
    SRM_Context_RegisterCleanup(srm_ctx1, SomeFunctionWithScopedResources_closeFile, f1, NULL);

    printf("Doing some job\n");

    // Closing scope, this will trigger all resource cleanup and shutdown callbacks.
    SRM_Context_CloseScope(srm_ctx1);
}

